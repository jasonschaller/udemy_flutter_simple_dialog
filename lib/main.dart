import 'package:flutter/material.dart';

void main () {
  runApp(new MaterialApp(home: new application()));
}

class application extends StatefulWidget {
  @override
  _applicationState createState() => _applicationState();
}

class _applicationState extends State<application> {

  SimpleDialog _sb;

  void DialogMethod() {
    _sb = new SimpleDialog(
      title: new Text('Title'),
      children: <Widget>[
        new SimpleDialogOption(
          child: new Text('Yes'),
          onPressed: () {print('Yes');},
        ),
        new SimpleDialogOption(
          child: new Text('No'),
          onPressed: () {print('No');},
        ),
        new SimpleDialogOption(
          child: new Text('Close'),
          onPressed: () {Navigator.pop(context);},
        ),
      ],
    );
    showDialog(context: context, child: _sb);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new RaisedButton(
            onPressed: () {DialogMethod();},
            child: new Text('Simple dialog'),
        ),
      )
    );
  }
}
